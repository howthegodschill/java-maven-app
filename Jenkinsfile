#!/usr/bin/env groovy

pipeline {
    agent any
    tools {
        maven 'maven-3.8'
    }
    stages {
        stage('increment version') {
            steps{
                script {
                    echo 'incrementing app version...'
                    sh 'mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion}. \
                        versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]
                    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('build app') {
            steps {
                script {
                    echo "gotta clean up..."
                    sh 'mvn clean package'
                }
            }
        }
        stage('build image') {
            steps {
                script {
                    echo "building the Docker image..."
                    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh "docker build -t howthegodschill/demo-app:${IMAGE_NAME} ."
                        sh "echo $PASS | docker login -u $USER --password-stdin"
                        sh "docker push howthegodschill/demo-app:${IMAGE_NAME}"
                    }
                }
            }
        }
        stage('deploy') {
            steps {
                script {
                    echo "deploying docker image to my personal private Docker Hub repo..."
                }
            }
        }
        stage('commit version update') {
            steps {
                script {
                    echo "building the Docker image..."
                    withCredentials([usernamePassword(credentialsId: 'personal-gitlab', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh 'git config user.email "jenkins@example.com"'
                        sh 'git config user.name "Jenkins"'
                        
                        sh 'git status'
                        sh 'git branch'
                        sh 'git config --list'

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/howthegodschill/java-maven-app.git"
                        sh 'git add .'
                        sh 'git commit -m "Jenkins CI Version Bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}